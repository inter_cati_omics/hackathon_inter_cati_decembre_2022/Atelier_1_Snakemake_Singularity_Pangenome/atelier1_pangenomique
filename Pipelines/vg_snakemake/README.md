# WGS SNPs calling pipeline for Paired-end Illumina sequencing on pangenome graph


## From raw fastq data to vcf file (without genotype!!!!).
## Pipeline features:
### 1) read preprocessing,
### 2) reads QC, fastq report
### 4) mapping with vg giraffe,
### 5) SNPs calling with vg

:exclamation: :exclamation: !! For the reference fasta file, he reference definition line must not contain space or special character.


### Snakemake features: fastq from csv file, config, modules, SLURM

#### Workflow steps are descibed in the dag_rules.pdf


### Files description:
### 1) Snakefile
    - Snakefile_vg.smk

### 2) Configuration file in yaml format:, paths, singularity images paths, parameters, filters ....
    - config.yaml

### 3) a sbatch file to run the pipeline: (to be edited)
    - run_snakemake_vg.slurm

### 4) A slurm directive (#core, mem,...) in json format. Can be adjusted if needed
    - cluster.json

### 5) samples file in csv format
    Must contens at least 2 columns for SE reads and 3 for PE reads (tab separator )
    SampleName  fq1     fq2
    SampleName : your sample ID
    fq1: fastq file for a given sample
    fq2: read 2 for paired-end reads
    (sp1   ra_R1.fastq.gz   ra_R2.fastq.gz)

    a sample my have multiple fastq files separated by a ','
    (sp1   ra_R1.fastq.gz,rb_R1.fastq.gz   ra_R2.fastq.gz,rb_R2.fastq.gz)

    - samples.csv


## RUN:

### 1) Edit the config.yaml

### 2) Set your samples in the sample.csv

### 3) Adjust the run_snakemake_pipeline_gatk.slurm file

### 3) Run pipelene in dry run mode first:
`sbatch run_snakemake_vg.slurm`

### 4) uncomment the real run line and run the pipeline:
`sbatch run_snakemake_vg.slurm`


#### Documentation being written (still)


