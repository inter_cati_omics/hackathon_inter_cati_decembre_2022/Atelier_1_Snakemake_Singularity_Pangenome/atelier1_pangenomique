#!/bin/bash

#samtools
singularity pull samtools_v1.14.sif oras://registry.forgemia.inra.fr/inter_cati_omics/hackathon_inter_cati_decembre_2022/atelier_1_snakemake_singularity_pangenome/containers/samtools/samtools:latest

#vg
singularity pull vg_v1.44.0.sif oras://registry.forgemia.inra.fr/inter_cati_omics/hackathon_inter_cati_decembre_2022/atelier_1_snakemake_singularity_pangenome/containers/vg/vg:latest


# multiqc
singularity pull multiqc_v1.13.sif oras://registry.forgemia.inra.fr/inter_cati_omics/hackathon_inter_cati_decembre_2022/atelier_1_snakemake_singularity_pangenome/containers/multiqc/multiqc:latest



