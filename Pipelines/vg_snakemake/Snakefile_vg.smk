__author__ = "INRAE"
__license__ = "MIT"
__copyright__ = "INRAE, 2022"

import os
import sys
import pandas as pd
from snakemake.utils import min_version
# snakemake built-in report function requires min version 5.1
min_version("5.1.0")

#############################################################################
#
#     vg pipeline for PE reads
#     1) preprocessing and quality control
#     2) graph idexing
#     3) mapping using vg and alignement stats
#     4) SNPs calling using vg
#
#############################################################################

# Read the sample file using pandas lib (sample names+ fastq names) and create index using the sample name
samples = pd.read_csv(config["samplesfile"], sep='\t', dtype=str, comment='#').set_index(["SampleName"], drop=False)

# Get fastq R1. Could have several fastq file for 1 sample
def get_fql1(wildcards):
    tt=samples.loc[(wildcards.sample), ["fq1"]].dropna()
    fs=re.split('[ ;,]+',tt.item())
    ml=list()
    for f in fs:
      f.strip()
      ml.append(config["fq_dir"]+"/"+f)
    return ml

# Get fastq R2. Could have several fastq file for 1 sample
def get_fql2(wildcards):
    tt=samples.loc[(wildcards.sample), ["fq2"]].dropna()
    fs=re.split('[ ;,]+',tt.item())
    ml=list()
    for f in fs:
      f.strip()
      ml.append(config["fq_dir"]+"/"+f)
    return ml


####################################################################################
############################ filan rule ############################################
####################################################################################

rule final_outs:
    input:
        "{outdir}/multiqc/multiqc_report_fastqc.html".format(outdir=config["outdir"]),
        expand("{outdir}/mapped/{sample}.gam", outdir=config["outdir"], sample=samples['SampleName']),
        expand("{outdir}/variant/{sample}.vcf", outdir=config["outdir"], sample=samples['SampleName']),
        #"{outdir}/multiqc/multiqc_report_gam.html".format(outdir=config["outdir"]),

##################################################################################
########################  PREPROCESSING  #########################################
##################################################################################

# 1-1) reads preprocessing in paired end mode :
#PREPROC
# Adaptor removal, trim PE read with cutadapt or fastp

rule fastp_pe:
    input:
        R1=get_fql1,
        R2=get_fql2,
    output:
        R1 = "{outdir}/fastp/{{sample}}_1_trim.fastq.gz".format(outdir=config["outdir"]),
        R2 = "{outdir}/fastp/{{sample}}_2_trim.fastq.gz".format(outdir=config["outdir"])
    message: "Running fastp on files \n"
    params:
        fqdir      = config["fq_dir"],
        outdir     = config["outdir"],
        fastp_bin  = config["fastp_bin"],
        bind       = config["BIND"],
        json       = config["outdir"]+"/fastp/{sample}_trim.json",
        html       = config["outdir"]+"/fastp/{sample}_trim.html"
    shell:
       """
        singularity exec {params.bind} {params.fastp_bin} fastp \
        --stdin \
        -i <(zcat {input.R1}) \
        -I <(zcat {input.R2}) \
        -o {output.R1} \
        -O {output.R2} \
        -h {params.html} \
        -j {params.json} \
        --max_len1 350 \
        --correction \
        --detect_adapter_for_pe \
        --cut_mean_quality 20 \
        --cut_window_size 4 \
        --low_complexity_filter \
        --complexity_threshold 30 \
        -w 4
       """

# 1-3) check quality control using FastQC: for each individual
#QC
#fastqc PE mode
#function return fastq1 and fastq2 files and adds the fastq path
rule fastqc_pe:
    input:
        R1 = "{outdir}/fastp/{{sample}}_1_trim.fastq.gz".format(outdir=config["outdir"]),
        R2 = "{outdir}/fastp/{{sample}}_2_trim.fastq.gz".format(outdir=config["outdir"])
    output:
        # to avoid the output name generated by fastqc (regex on the name) we use a flag file
        "{outdir}/fastqc/{{sample}}.OK.done".format(outdir=config["outdir"])
    threads:
        2
    params:
        fqdir      = config["fq_dir"],
        outdir     = config["outdir"],
        fastqc_bin = config["fastqc_bin"],
        bind       = config["BIND"]
    shell:
        """
        singularity exec {params.bind} {params.fastqc_bin} fastqc -o {params.outdir}/fastqc -t {threads} {input.R1} {input.R2} && touch {output}
        """


# 1-4) check quality control using FastQC: a global multiQC for the analysis
# multiQC on fastqc outputs
rule multiqc_fastqc:
    input:
        expand("{outdir}/fastqc/{sample}.OK.done", outdir=config["outdir"], sample=samples['SampleName'])
    output:
        #report("{outdir}/multiqc/multiqc_report_fastqc.html".format(outdir=config["outdir"]), caption="report/multiqc.rst", category="Quality control")
        "{outdir}/multiqc/multiqc_report_fastqc.html".format(outdir=config["outdir"])
    threads:
        1
    params:
        outdir      = config["outdir"],
        multiqc_bin = config["multiqc_bin"],
        bind        = config["BIND"]
    shell:
        """
        singularity exec {params.bind} {params.multiqc_bin} multiqc --filename {output} {params.outdir}/fastqc
        """

###############################################################################
########################  graph mapping   #######################################
###############################################################################

# 2-1) reference indexation
rule vg_autoindex_giraffe:
    input:
        gfa = config["REFPATH"] + "/" + config["GFA"]
    output:
        gfaidx   = "{outdir}/index/{idxp}.vg".format(outdir=config["outdir"],idxp=config["IDXPREFIX"]),
        #.giraffe.gbz -m pggb.min -d pggb.dist
    params:
        pref         = "{outdir}/index/{p}".format(outdir=config["outdir"],p=config["IDXPREFIX"]),
        bind         = config["BIND"],
        vg_bin      = config["vg_bin"],
        samtools_bin = config["samtools_bin"]
    shell:
        """
        singularity exec {params.bind} {params.vg_bin} vg autoindex -g {input.gfa} -w giraffe -p {params.pref} && \
        singularity exec {params.bind} {params.vg_bin} vg convert -g {input.gfa} -v > {output.gfaidx}
        """

# 2-2) mapping
####################################################
#   Requirement:
#       - gfa indexed
#       - trimmed reads in config["outdir"]/fastp/
#   Input:
#       - config["REFPATH"] and config["GENOME"]
#       -{sample}_1_trim.fastq.gz + {sample}_2_trim.fastq.gz
#   Output:
#       - {sample}.gam
# ############################################################
rule mapping_giraffe_pe :
    input:
        R1 = "{outdir}/fastp/{{sample}}_1_trim.fastq.gz".format(outdir=config["outdir"]),
        R2 = "{outdir}/fastp/{{sample}}_2_trim.fastq.gz".format(outdir=config["outdir"]),
        idx = "{outdir}/index/{idxp}.vg".format(outdir=config["outdir"],idxp=config["IDXPREFIX"]),
    output:
        gam   = "{outdir}/mapped/{{sample}}.gam".format(outdir=config["outdir"]),
    params:
        pref         = "{outdir}/index/{p}".format(outdir=config["outdir"],p=config["IDXPREFIX"]),
        outtmp       = "{outdir}/mapped/{{sample}}".format(outdir=config["outdir"]),
        bind         = config["BIND"],
        vg_bin      = config["vg_bin"],
        samtools_bin = config["samtools_bin"],
    threads: 8
    shell:
        """
        singularity exec {params.bind} {params.vg_bin} vg giraffe \
        -Z {params.pref}.giraffe.gbz -m {params.pref}.min -d {params.pref}.dist \
        -f {input.R1} -f {input.R2} \
        --named-coordinates > {output.gam}
        """

rule pack_giraffe :
    input:
        gam   = "{outdir}/mapped/{{sample}}.gam".format(outdir=config["outdir"]),
        idx = "{outdir}/index/{idxp}.vg".format(outdir=config["outdir"],idxp=config["IDXPREFIX"]),
    output:
        pack   = "{outdir}/mapped/{{sample}}.pack".format(outdir=config["outdir"]),
    params:
        pref         = "{outdir}/index/{p}".format(outdir=config["outdir"],p=config["IDXPREFIX"]),
        outtmp       = "{outdir}/mapped/{{sample}}".format(outdir=config["outdir"]),
        bind         = config["BIND"],
        vg_bin      = config["vg_bin"],
        samtools_bin = config["samtools_bin"],
    threads: 8
    shell:
        """
        singularity exec {params.bind} {params.vg_bin} vg pack \
        -x {input.idx} \
        -g {input.gam} \
        -d \
        -Q 5 -s 5 \
        -o {output.pack}
        """


rule calling_giraffe :
    input:
        pack   = "{outdir}/mapped/{{sample}}.pack".format(outdir=config["outdir"]),
        idx    = "{outdir}/index/{idxp}.vg".format(outdir=config["outdir"],idxp=config["IDXPREFIX"]),
    output:
        vcf    = "{outdir}/variant/{{sample}}.vcf".format(outdir=config["outdir"]),
    params:
        pref    = "{outdir}/index/{p}".format(outdir=config["outdir"],p=config["IDXPREFIX"]),
        bind    = config["BIND"],
        vg_bin  = config["vg_bin"],
    threads: 8
    shell:
        """
        singularity exec {params.bind} {params.vg_bin} vg call \
        -a {input.idx} \
        -k {input.pack} > {output.vcf}
        #vg call -a pggb_p1g1.vg -k pop1_g1.pack > pop1_g1.vcf
        """



# 2-4) bam stats
rule gam_stats:
    input:
        gam   = "{outdir}/mapped/{{sample}}.gam".format(outdir=config["outdir"])
    output:
        stats = "{outdir}/mapped/{{sample}}.gam.stats.txt".format(outdir=config["outdir"])
    params:
        outdir       = config["outdir"],
        bind         = config["BIND"],
        vg_bin      = config["vg_bin"],
        samtools_bin = config["samtools_bin"]
    threads: 1
    shell:
        """
        singularity exec {params.bind} {params.vg_bin} vg stats -a {input.gam} > {output.stats}
        """


# 2-5) multiqc for bams
rule multiqc_gam:
    input:
        expand("{outdir}/mapped/{sample}.gam.stats.txt", outdir=config["outdir"], sample=samples['SampleName'])
    output:
        "{outdir}/multiqc/multiqc_report_gam.html".format(outdir=config["outdir"])
        #"{outdir}/multiqc/multiqc_report.html".format(outdir=config["outdir"])
    threads:
        1
    params:
        outdir      = config["outdir"]+"/mapped/*.stats.txt",
        multiqc_bin = config["multiqc_bin"],
        bind        = config["BIND"]
    shell:
        """
        #mkdir -p {params.outdir}/multiqc #snakemake create automaticly the folders
        singularity exec {params.bind} {params.multiqc_bin} multiqc --filename {output} {input}
        """




################################################################################
###########################  SNP calling usingvg ###########################
################################################################################


# recalibration need vcf reference
#rule gatk4_recalibrate_calls:

#to compress & index a vcf:
#bgzip file.vcf       # or:   bcftools view file.vcf -Oz -o file.vcf.gz
#tabix file.vcf.gz    # or:   bcftools index file.vcf.gz

######################################## END GATK #################################################33

