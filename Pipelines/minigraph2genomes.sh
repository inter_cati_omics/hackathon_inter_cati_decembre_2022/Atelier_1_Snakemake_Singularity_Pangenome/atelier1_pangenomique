#!/bin/bash

#SBATCH --job-name=minigraph_2g # job name (-J)
#SBATCH --time="00-01:00:00" #max run time "hh:mm:ss" or "dd-hh:mm:ss" (-t)
#SBATCH --cpus-per-task=20 # max nb of cores (-c)
#SBATCH --ntasks=1 #nb of tasks
#SBATCH --mem=32G # max memory (-m)
#SBATCH --output=minigraph_2genomes.out #stdout (-o)

## for singularity on genotoul
module unload system/singularity*
module load system/singularity-3.7.3
#####################################

export SINGULARITY_BINDPATH="/work/project/gafl"

singularity pull --force minigraph_v0.20.sif oras://registry.forgemia.inra.fr/inter_cati_omics/hackathon_inter_cati_decembre_2022/atelier_1_snakemake_singularity_pangenome/containers/minigraph/minigraph:latest


minigraph="./minigraph_v0.20.sif"

gp="../data"

g1=g1.chr8.fasta.gz
g2=g2.chr8.fasta.gz

$minigraph -cxggs -t20 $gp/$g1 $gp/$g2 -o g1g2.gfa

#$minigraph -cxggs -t20 <$(zcat $gp/$g1) <$(zcat $gp/$g2) -o g1g2.gfa
