#!/bin/bash

#SBATCH --job-name=pggb_2genomes # job name (-J)
#SBATCH --time="00-01:00:00" #max run time "hh:mm:ss" or "dd-hh:mm:ss" (-t)
#SBATCH --cpus-per-task=20 # max nb of cores (-c)
#SBATCH --ntasks=1 #nb of tasks
#SBATCH --mem=64G # max memory (-m)
#SBATCH --output=pggb_2genomes.out #stdout (-o)

## for singularity on genotoul
module unload system/singularity*
module load system/singularity-3.7.3
#####################################

export SINGULARITY_BINDPATH="/work/project/gafl"

singularity pull --force pggb_v0.5.1.sif oras://registry.forgemia.inra.fr/inter_cati_omics/hackathon_inter_cati_decembre_2022/atelier_1_snakemake_singularity_pangenome/containers/pggb/pggb:latest
singularity pull --force samtools_v1.16.1.sif oras://registry.forgemia.inra.fr/inter_cati_omics/hackathon_inter_cati_decembre_2022/atelier_1_snakemake_singularity_pangenome/containers/samtools/samtools:latest


pggb="./pggb_v0.5.1.sif"
samtools="singularity exec samtools_v1.16.1.sif"

gp="../data"

g1=g1.chr8.fasta.gz
g2=g2.chr8.fasta.gz

#zcat  $gp/$g1 |sed '/^>/ s/^/ref_/' >g1g2.fa
#zcat  $gp/$g2 >>g1g2.fa

# no compression
#zcat $gp/$g1 $gp/$g2 >g1g2.fa
#$samtools samtools faidx g1g2.fa

# with bgzip
zcat $gp/$g1 $gp/$g2 |$samtools bgzip -@ 10 >g1g2.fa.gz
$samtools samtools faidx g1g2.fa.gz


$pggb -i g1g2.fa.gz \
-o pggb_g1g2_output \
-n 2 \
-t 20 \
-p 90 \
-m \
-s 5k \
-V 'g1_chr8:#:1000'

