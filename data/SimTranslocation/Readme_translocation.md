# Translocation definition

## individual
g1.chr8_2chr_transloc.fasta

## Methodology
- for each individual, split the single chromosome 8 into two chromosomes (8A & 8B) by "agrep" the following sequence "GCAGAACTGGTGGCAACGAAACG"
- add telomers  to split site by using 5' and 3' sequence of the one of the individual
- build the simulated translocation

## Coordinate definition
### initial position
  - chr: g1_chr8.A
  - start: pos: 1829877 ; line - 30000 ; seq - "AACGAAAGTCATGGTCATGAAAACTGTTTGTCGAATAAGCGTCATGGTTTTGAACAATGT"
  - stop: line - 30180 ; seq - "GGTTGTTGGTCAGGACGGGTGCAAGAACCCGTAAAGTTCGAGAATGCCGTACTCACTCAA"
  - length: 11040bp

### new position
  - chr: g1_chr8.B
  - start: pos - 12016 ; line 30758 ; seq - "AACGAAAGTCATGGTCATGAAAACTGTTTGTCGAATAAGCGTCATGGTTTTGAACAATGT"
  - stop: line 30938 ; seq - "GGTTGTTGGTCAGGACGGGTGCAAGAACCCGTAAAGTTCGAGAATGCCGTACTCACTCAA"
  - length: 11040bp
