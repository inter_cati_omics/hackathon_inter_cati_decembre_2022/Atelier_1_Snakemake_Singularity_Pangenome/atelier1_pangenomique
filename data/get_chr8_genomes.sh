#!/bin/bash

for i in g1.chr8.fasta g2.chr8.fasta ST99CH_1A5.chr8.fasta ST99CH_3D7.chr8.fasta ZT1E4.chr8.fasta ZT3D1.chr8.fasta ztIPO323.chr8.fasta
do
    echo $i
    wget --no-check-certificate https://bioinfo.bioger.inrae.fr/portal/data-browser/misc/hackathon_cati_2022/data/$i
    gzip $i
done


for i in g1.chr8.fasta g2.chr8.fasta ST99CH_1A5.chr8.fasta ST99CH_3D7.chr8.fasta ZT1E4.chr8.fasta ZT3D1.chr8.fasta ztIPO323.chr8.fasta
do
	zcat ${i}.gz|grep '^>'
done



#>g1_chr8
#>g2_chr8
#>ST99CH_1A5_chr8
#>ST99CH_3D7_chr8
#>ZT1E4_chr8
#>ZT3D1_chr8
#>chr_8


exit 0



