# atelier1_pangenomique

#### Un channel Mattermost a été créé:
https://team.forgemia.inra.fr/catiomics/channels/aussois_2022_atelier1

#### Restitution de l'atelier:
https://forgemia.inra.fr/inter_cati_omics/hackathon_inter_cati_decembre_2022/Atelier_1_Snakemake_Singularity_Pangenome/atelier1_pangenomique/-/blob/main/20221208_Hackathon_Restitution_AtelierPange%CC%81nomique.pptx

#### CR du 29/11/2022
https://docs.google.com/document/d/1ASvsncTHmXE-dRSgOcoNl2m_70ibK-TuYGXY7ORC7YE/edit?usp=sharing


#### data:
https://bioinfo.bioger.inrae.fr/portal/data-browser/misc/hackathon_cati_2022/data

Les génomes et les fastq (avec wget scripts) sont mis dans data

Correspondance des chromo avec mummer vs IPO323:
https://bioinfo.bioger.inrae.fr/portal/data-browser/misc/hackathon_cati_2022/mummer



#### un pad: framapad
https://semestriel.framapad.org/p/xvufh4rafm_hackathon_2022_atelier1-9xx8



### Liens tools:

vg:
https://github.com/vgteam/vg
https://github.com/vgteam/vg.wiki.git


GFAffix:
https://github.com/marschall-lab/GFAffix

minigraph:
https://github.com/lh3/minigraph

pggb:
https://github.com/pangenome/pggb

cactus:
https://github.com/ComparativeGenomicsToolkit/cactus/blob/master/README.md

Minigraph-cactus pangenome pipeline:
https://github.com/ComparativeGenomicsToolkit/cactus/blob/master/doc/pangenome.md

gfatools:
https://github.com/lh3/gfatools

nf-core pangenome:
https://github.com/nf-core/pangenome


